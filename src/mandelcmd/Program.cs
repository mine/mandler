﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MandelbrotLib;

namespace mandelcmd
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 6)
            {
                int width, height, iterations;
                double x, y, zoom;
                try
                {
                    width = int.Parse(args[0]);
                    height = int.Parse(args[1]);
                    x = double.Parse(args[2]);
                    y = double.Parse(args[3]);
                    zoom = double.Parse(args[4]);
                    iterations = int.Parse(args[5]);
                }
                catch
                {
                    Console.WriteLine("\nParameter error");
                    return;
                }
                Mandelbrot m = new Mandelbrot(width, height, x, y, zoom, iterations);
                m.Coloring = MandelColor.Green;
                m.SaveFractalAntiAliased("mandelbrot.png");
            }
            else
            {
                Console.WriteLine("\nUsage: \n    mandelcmd <width:int> <height:int> <x:float> <y:float> <zoom:float> <iterations:int>\n\n");
            }
        }
    }
}
