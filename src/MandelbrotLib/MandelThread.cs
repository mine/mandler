﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;

namespace MandelbrotLib
{
    class MandelThread
    {
        private double x, cX, cY, zoom;
        private int height, iterations;
        private Color[] cols;
        private MandelColor c;

        private Thread thread;

        public MandelThread(double x, int height, double centerX, double centerY, double zoom, Color[] colors, int iterations, MandelColor col)
        {
            this.x = x;
            this.cX = centerX;
            this.cY = centerY;
            this.height = height;
            this.zoom = zoom;
            this.cols = colors;
            this.iterations = iterations;
            this.c = col;

            this.thread = new Thread(makeRow);
        }

        private double mandel(double re, double im)
        {
            int i = 0;
            double x_neu = 0, y_neu = 0, x_alt = 0, y_alt = 0;

            while (Math.Pow(x_neu, 2) + Math.Pow(y_neu, 2) <= 4 && i < iterations)
            {
                x_alt = x_neu;
                y_alt = y_neu;
                x_neu = Math.Pow(x_alt, 2) - Math.Pow(y_alt, 2) + re;
                y_neu = 2 * x_alt * y_alt + im;
                i++;
            }
            if (i < iterations)
            {
                return i - Math.Log(Math.Log(Math.Sqrt(x_neu * x_neu + y_neu * y_neu), 2), 2);
            }
            return i;
        }

        private void makeRow()
        {
            double X = x / zoom + cX, Y;
            for (int y = 0; y < height; y++)
            {
                Y = y / zoom + cY;
                cols[y] = MandelTools.PickColor(mandel(X, Y), iterations, c);
            }
        }

        public void Start()
        {
            thread.Start();
        }

        public void Abort()
        {
            thread.Abort();
        }

        public void Join()
        {
            thread.Join();
        }
    }
}
