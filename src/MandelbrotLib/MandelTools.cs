﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace MandelbrotLib
{
    class MandelTools
    {
        private static Color palettePseudocolor(double i, double iterations, Color[] palette)
        {
            int max_index = palette.Length - 1;

            double v = i / iterations * max_index;
            int k = (int)v;
            double f = v - k;

            Color c0 = palette[k], c1 = palette[Math.Min(k + 1, max_index)];

            double r = c1.R - c0.R, g = c1.G - c0.G, b =c1.B - c0.B;
            return Color.FromArgb((int)(c0.R + f * r), (int)(c0.G + f * g), (int)(c0.B + f * b));
        }

        public static Color PickColor(double i, int iterations, MandelColor type)
        {
            double ratio = i / iterations;
            switch (type)
            { 
                case MandelColor.Red:
                    return palettePseudocolor(i, (double)iterations, new Color[8] { 
                        ColorTranslator.FromHtml("#160200"), 

                        ColorTranslator.FromHtml("#d54b15"), 
                        ColorTranslator.FromHtml("#961200"), 
                        ColorTranslator.FromHtml("#fda659"),

                        ColorTranslator.FromHtml("#fd8859"), 
                        ColorTranslator.FromHtml("#bd3b18"), 
                        ColorTranslator.FromHtml("#961200"), 

                        Color.Black });

                case MandelColor.Green:
                    return palettePseudocolor(i, (double)iterations, new Color[8] { 
                        ColorTranslator.FromHtml("#0b1600"), 

                        ColorTranslator.FromHtml("#8ad515"), 
                        ColorTranslator.FromHtml("#4e9600"), 
                        ColorTranslator.FromHtml("#7cfd59"),

                        ColorTranslator.FromHtml("#bafd59"), 
                        ColorTranslator.FromHtml("#8cbd18"), 
                        ColorTranslator.FromHtml("#6a9600"), 

                        Color.Black });

                case MandelColor.Blue:
                    return palettePseudocolor(i, (double)iterations, new Color[8] { 
                        ColorTranslator.FromHtml("#000816"), 

                        ColorTranslator.FromHtml("#156bd5"), 
                        ColorTranslator.FromHtml("#002e96"), 
                        ColorTranslator.FromHtml("#5980fd"),

                        ColorTranslator.FromHtml("#5984fd"), 
                        ColorTranslator.FromHtml("#184abd"), 
                        ColorTranslator.FromHtml("#003896"), 

                        Color.Black });

                default:
                    return palettePseudocolor(i, (double)iterations, new Color[8] { 
                        ColorTranslator.FromHtml("#161616"), 

                        ColorTranslator.FromHtml("#d5d5d5"), 
                        ColorTranslator.FromHtml("#969696"), 
                        ColorTranslator.FromHtml("#fdfdfd"),

                        ColorTranslator.FromHtml("#fdfdfd"), 
                        ColorTranslator.FromHtml("#bdbdbd"), 
                        ColorTranslator.FromHtml("#969696"), 

                        Color.Black });
            }
        }

        public static Image ResizeImage(Image img, Size size)
        {
            Bitmap bmp = new Bitmap(size.Width, size.Height);
            Graphics graph = Graphics.FromImage(bmp);
            graph.InterpolationMode = InterpolationMode.High;
            graph.CompositingQuality = CompositingQuality.HighQuality;
            graph.SmoothingMode = SmoothingMode.AntiAlias;
            graph.DrawImage(img, new Rectangle(0, 0, size.Width, size.Height));
            return bmp;
        }
    }
}
