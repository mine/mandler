﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace MandelbrotLib
{
    public class Mandelbrot
    {
        private int width, height, iterations; 
        private double reC, imC, zoom;

        public MandelColor Coloring 
        {
            get;
            set;
        }

        public double Progress
        {
            get;
            private set;
        }

        public Mandelbrot(int width, int height, double centerX, double centerY, double zoom, int iterations)
        {
            this.width = width;
            this.height = height;

            this.reC = centerX;
            this.imC = centerY;

            this.zoom = zoom;
            this.iterations = iterations;

            this.Progress = 0;
        }


        public Image CreateFractal()
        {
            this.Progress = 0;

            Image output = new Bitmap(width, height);
            Graphics im = Graphics.FromImage(output);

            #region move center to upper left corner
            double maxRe = width / zoom;
            double centerRe = reC - maxRe * 0.5;

            double maxIm = height / zoom;
            double centerIm = -imC - maxIm * 0.5;
            #endregion


            MandelThread[] threads = new MandelThread[width];
            List<Color[]> colors = new List<Color[]>();
            for (int x = 0; x < width; x++)
            {
                colors.Add(new Color[height]);
                threads[x] = new MandelThread(x, height, centerRe, centerIm, zoom, colors[x], this.iterations, this.Coloring);
            }

            double percent = 100.0 / width / 2;
            foreach (MandelThread item in threads)
            {
                this.Progress += percent;
                item.Start();
            }
            foreach (MandelThread item in threads)
            {
                item.Join();
                this.Progress += percent;
            }

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    im.DrawRectangle(new Pen(colors[x][y]), new Rectangle(x, y, 1, 1));
                }
            }
            return output;
        }

        public Image CreateFractalAntiAliased()
        {
            this.width *= 2;
            this.height *= 2;
            this.zoom *= 2;

            Image o = MandelTools.ResizeImage(CreateFractal(), new Size(this.width / 2, this.height / 2));
            
            this.width /= 2;
            this.height /= 2;
            this.zoom /= 2.0;

            return o;
        }

        public void SaveFractal(string filename)
        {
            CreateFractal().Save(filename);
        }

        public void SaveFractalAntiAliased(string filename)
        {
            CreateFractalAntiAliased().Save(filename);
        }
    }
}
