﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MandelbrotLib
{
    public enum MandelColor
    {
        Red, 
        Green, 
        Blue, 
        Grey
    }
}
