﻿namespace mandelgui
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.viewer = new System.Windows.Forms.PictureBox();
            this.zoomIn_btn = new System.Windows.Forms.Button();
            this.zoomOut_btn = new System.Windows.Forms.Button();
            this.save = new System.Windows.Forms.Button();
            this.progress = new System.Windows.Forms.ProgressBar();
            this.checkProgress = new System.Windows.Forms.Timer(this.components);
            this.Reset = new System.Windows.Forms.Button();
            this.antialias = new System.Windows.Forms.Button();
            this.colorPicker = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.viewer)).BeginInit();
            this.SuspendLayout();
            // 
            // viewer
            // 
            this.viewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.viewer.Location = new System.Drawing.Point(0, 0);
            this.viewer.Name = "viewer";
            this.viewer.Size = new System.Drawing.Size(584, 562);
            this.viewer.TabIndex = 0;
            this.viewer.TabStop = false;
            this.viewer.DoubleClick += new System.EventHandler(this.viewer_DoubleClick);
            this.viewer.MouseMove += new System.Windows.Forms.MouseEventHandler(this.viewer_MouseMove);
            // 
            // zoomIn_btn
            // 
            this.zoomIn_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zoomIn_btn.Location = new System.Drawing.Point(519, 12);
            this.zoomIn_btn.Name = "zoomIn_btn";
            this.zoomIn_btn.Size = new System.Drawing.Size(27, 26);
            this.zoomIn_btn.TabIndex = 1;
            this.zoomIn_btn.Text = "+";
            this.zoomIn_btn.UseVisualStyleBackColor = true;
            this.zoomIn_btn.Click += new System.EventHandler(this.zoomIn_btn_Click);
            // 
            // zoomOut_btn
            // 
            this.zoomOut_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zoomOut_btn.Location = new System.Drawing.Point(552, 12);
            this.zoomOut_btn.Name = "zoomOut_btn";
            this.zoomOut_btn.Size = new System.Drawing.Size(27, 26);
            this.zoomOut_btn.TabIndex = 2;
            this.zoomOut_btn.Text = "-";
            this.zoomOut_btn.UseVisualStyleBackColor = true;
            this.zoomOut_btn.Click += new System.EventHandler(this.zoomOut_btn_Click);
            // 
            // save
            // 
            this.save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.save.Location = new System.Drawing.Point(519, 66);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(59, 26);
            this.save.TabIndex = 3;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // progress
            // 
            this.progress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.progress.Location = new System.Drawing.Point(519, 44);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(59, 16);
            this.progress.TabIndex = 4;
            this.progress.Value = 50;
            // 
            // checkProgress
            // 
            this.checkProgress.Interval = 10;
            this.checkProgress.Tick += new System.EventHandler(this.checkProgress_Tick);
            // 
            // Reset
            // 
            this.Reset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Reset.Location = new System.Drawing.Point(519, 98);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(59, 26);
            this.Reset.TabIndex = 5;
            this.Reset.Text = "Reset";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // antialias
            // 
            this.antialias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.antialias.Location = new System.Drawing.Point(519, 130);
            this.antialias.Name = "antialias";
            this.antialias.Size = new System.Drawing.Size(59, 26);
            this.antialias.TabIndex = 6;
            this.antialias.Text = "Antialias";
            this.antialias.UseVisualStyleBackColor = true;
            this.antialias.Click += new System.EventHandler(this.antialias_Click);
            // 
            // colorPicker
            // 
            this.colorPicker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.colorPicker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.colorPicker.FormattingEnabled = true;
            this.colorPicker.Items.AddRange(new object[] {
            "Red",
            "Green",
            "Blue",
            "Grey"});
            this.colorPicker.Location = new System.Drawing.Point(519, 162);
            this.colorPicker.Name = "colorPicker";
            this.colorPicker.Size = new System.Drawing.Size(59, 21);
            this.colorPicker.TabIndex = 7;
            this.colorPicker.SelectionChangeCommitted += new System.EventHandler(this.colorPicker_SelectionChangeCommitted);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 562);
            this.Controls.Add(this.colorPicker);
            this.Controls.Add(this.antialias);
            this.Controls.Add(this.Reset);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.save);
            this.Controls.Add(this.zoomOut_btn);
            this.Controls.Add(this.zoomIn_btn);
            this.Controls.Add(this.viewer);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mandler by mine";
            ((System.ComponentModel.ISupportInitialize)(this.viewer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox viewer;
        private System.Windows.Forms.Button zoomIn_btn;
        private System.Windows.Forms.Button zoomOut_btn;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.Timer checkProgress;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Button antialias;
        private System.Windows.Forms.ComboBox colorPicker;
    }
}

