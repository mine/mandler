﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using MandelbrotLib;

namespace mandelgui
{
    public partial class Form1 : Form
    {
        private double zoom = 100, x = 0, y = 0;
        private int iterations = 500;
        private Mandelbrot frac = new Mandelbrot(0, 0, 0, 0, 0, 0);

        public Form1()
        {
            InitializeComponent();
            colorPicker.SelectedIndex = 2;
            checkProgress.Start();
            redraw();
        }
        
        private double[] getCoords(int sx, int sy)
        {
            double[] o = new double[2];

            int w = this.Width, h = this.Height;

            double digressLR = w / zoom * 0.5;
            double digressUD = h / zoom * 0.5;

            double shiftCenterCursor_X = sx - (w / 2.0); //shift of cursor to center
            double shiftCenterCursor_X_percentage = shiftCenterCursor_X / (w / 2.0); //shift in percentage
            o[0] = x + digressLR * shiftCenterCursor_X_percentage; //new position

            double shiftCenterCursor_Y = sy - (h / 2.0);
            double shiftCenterCursor_Y_percentage = shiftCenterCursor_Y / (h / 2.0);
            o[1] = y - digressUD * shiftCenterCursor_Y_percentage;

            return o;
        }

        private void viewer_DoubleClick(object sender, EventArgs e)
        {
            MouseEventArgs m = e as MouseEventArgs;
            double[] c = getCoords(m.X, m.Y);
            x = c[0];
            y = c[1];
            zoom *= 2;
            redraw();
        }

        #region visible actions

        private void viewer_MouseMove(object sender, MouseEventArgs e)
        {
            double[] pos = getCoords(e.X, e.Y);
            Text = "Mandler by mine " + pos[0] + ";" + pos[1];
        }

        #region button actions
        private void save_Click(object sender, EventArgs e)
        {
            SaveFileDialog dia = new SaveFileDialog();
            dia.Filter = "PNG-File|*.png";
            if (dia.ShowDialog() == DialogResult.OK)
                viewer.Image.Save(dia.FileName);
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            x = y = 0;
            zoom = 100;
            redraw();
        }

        private void zoomIn_btn_Click(object sender, EventArgs e)
        {
            zoom *= 2;
            redraw();
        }

        private void zoomOut_btn_Click(object sender, EventArgs e)
        {
            zoom /= 2;
            redraw();
        }
        #endregion

        private void checkProgress_Tick(object sender, EventArgs e)
        {
            progress.Value = (int)Math.Round(frac.Progress);
        }

        #region antialiasing
        private void antialiasImage()
        {
            guiEnabled(false);
            frac = new Mandelbrot(this.Width, this.Height, x, y, zoom, iterations);
            frac.Coloring = (MandelColor)Enum.Parse(typeof(MandelColor), colorPicker.Text); 
            viewer.Image = frac.CreateFractalAntiAliased();
            guiEnabled(true);
        }

        private void antialias_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(antialiasImage);
            t.Start();
        }
        #endregion

        #region normal redraws
        private void redraw()
        {
            Thread t = new Thread(redrawNormal);
            t.Start();
        }

        private void redrawNormal()
        {
            guiEnabled(false);
            frac = new Mandelbrot(this.Width, this.Height, x, y, zoom, iterations);
            frac.Coloring = (MandelColor)Enum.Parse(typeof(MandelColor), colorPicker.Text); 
            viewer.Image = frac.CreateFractal();
            guiEnabled(true);
        }
        #endregion

        private void guiEnabled(bool state)
        {
            zoomIn_btn.Enabled = zoomOut_btn.Enabled = save.Enabled = Reset.Enabled = antialias.Enabled = state;
            viewer.Enabled = state;
            colorPicker.Enabled = state;
        }
        #endregion

        private void colorPicker_SelectionChangeCommitted(object sender, EventArgs e)
        {
            redraw();
        }
    }
}
